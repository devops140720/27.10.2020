from flask import Flask
from flask import render_template, request, redirect, url_for

app = Flask(__name__)

@app.route('/')
def home_page():
    name = 'itay'
    return f'<html><h1 style="background-color:DodgerBlue;color:white"><b>Welcome to flask! {name}</b></h1>'+\
    '<table style="width:100%"><tr><th>Firstname</th><th>Lastname</th><th>Age</th>'+\
    '</tr><tr><td>Jill</td><td>Smith</td><td>50</td></tr><tr><td>Eve</td><td>Jackson</td>'+\
    '<td>94</td></tr></table></html>';

@app.route('/goodbye/<name>')
def goodbye(name):
    # name='itay' # 127.0.0.1:5000/goodbye/itay
    return f'<html><head><title>Goodbye</title></head><body>Goodbye {name}!</body></html>'

@app.route('/sumint/<int:x>/<int:y>')
def mysum(x, y):
    # x = 2 y = 5 # 127.0.0.1:5000/sum/2/5
    return f'<html><head><title>Calculator</title></head><body>{x} + {y} = {x+y}</body></html>'

@app.route('/sumfloat/<float:x>/<float:y>')
def mysumfloat(x, y):
    # x = 2 y = 5 # 127.0.0.1:5000/sum/2/5
    return f'<html><head><title>Calculator</title></head><body>{x} + {y} = {x+y}</body></html>'

@app.route('/divfloat/<float:x>/<float:y>')
def mydiv(x, y):
    if y == 0:
        return f'<html><head><title>Calculator - ERROR</title></head><body>'+\
               '<h1 style="background-color:red;color:white">Cannot divide by zero</h1></body></html>'
    else:
        return f'<html><head><title>Calculator</title></head><body>{x} / {y} = {x / y}</body></html>'

@app.route('/blog/<int:postID>')
def my_blog(postID):
    post = ['hello python','flask','docker','linux']
    return f'<html><body><h1>{post[postID]}</h1></body></html>'

@app.route('/admin')
def hello_admin():
    return f'<html><body><h1>Hello admin!</h1></body></html>'

@app.route('/user/<username>')
def hello_user(username):
    return f'<html><body><h1>Hello user {username}</h1></body></html>'

@app.route('/ynet')
def goto_ynet():
    return redirect('http://www.ynet.co.il')

@app.route('/login/<name>')
def login_me(name):
    if name == 'itay':
        return redirect(url_for('hello_admin'))
    else:
        return redirect(url_for('hello_user', username=name))

app.run()
print('Session ended........')
